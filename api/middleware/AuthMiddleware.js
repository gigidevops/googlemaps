const jwt = require("jsonwebtoken");
const UserService = require("../service/UserService");
const RoleService = require("../service/RoleService");

/**
 * Permet de vérifier si la requête demandé est accesible à l'appeleur de la requête. Via le token de celui-ci
 */
const checkDroit = (req, res, next) => {
    if(req.headers.authorization !== undefined){

        // Récupération des informations de la requête
        const token = req.headers.authorization.replace("Bearer ", "");
        const route = req.baseUrl.substr(1);
        const method = req.method;
        const decode = jwt.decode(token);

        if(decode !== null){
            const {id} = decode;

            // Récupération des droits du rôle
            UserService.getUserById(id).then(user => {
                RoleService.getRole(user.role).then(role => {
                    const {scope} = role;

                    const methods = scope[route];
                    if(methods !== undefined && methods.includes(method)){
                        next();
                    } else {
                        res.status("403").json({message: "Vous n'avez pas les droits"});
                    }
                }).catch(_ => res.status(400).json({message: "Impossible de récupérer le rôle de l'utilisateur"}));
            }).catch(_ => res.status(400).json({message: "Erreur lors de la récupération de l'utilisateur"}));
        } else {
            res.status("403").json({message: "Vous n'avez pas les droits"});
        }
    } else {
        res.status("403").json({message: "Vous n'avez pas les droits"});
    }
};

module.exports = {
    checkDroit
};