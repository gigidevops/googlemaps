const UserModel = require("../models/UserModel");
const UserService = require("../service/UserService");

const getUsers = async (req, res) => {
    const users = await UserService.getUsers();

    if(users === null){
        return res.status(400).json({message: "Impossible de récupérer tous les utilisateurs"});
    } else {
        return res.status(200).json(users);
    }
};

const getUser = async (req, res) => {
    const user = await UserService.getUserById(req.params.id);

    if(user === null){
        return res.status(400).json({message: "Impossible de récupérer l'utilisateur"});
    } else {
        return res.status(200).json(user);
    }
};

const addUser = async (req, res) => {
console.log(req.body.scope);

    // Récupérer toutes les données du formulaire
    const userData = UserModel({
        nom: req.body.nom,
        mail: req.body.mail,
        motDePasse: req.body.motDePasse,
        numeroDeSiret: req.body.numeroDeSiret,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        role: req.body.role
    });

    // Ajoute l'utilisateur
    const user = await UserService.addUser(userData);

    // Vérifie que l'ajout a réussi
    if(user === null){
        return res.status(400).json({message: "Erreur lors de la création de l'utilisateur"});
    } else {
        return res.status(200).json(user);
    }
};

const updateUser = async (req, res) => {
    // Récupérer toutes les données du formulaire
    const userData = {
        nom: req.body.nom,
        motDePasse: req.body.motDePasse,
        numeroDeSiret: req.body.numeroDeSiret,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        role: req.body.role
    };

    const user = await UserService.updateUser(req.params.id, userData);

    if(user === null){
        return res.status(400).json({ message: "Impossible de modifier l'utilisateur"});
    }else{
        return res.status(200).json(user);
    }
};

const deleteUser = async (req, res) => {
    const user = await UserService.deleteUser(req.params.id);

    if(user === null){
        return res.status(400).json({message: "Impossible de supprimer l'utilisateur"});
    } else {
        return res.status(200).json(user);
    }
};

module.exports = {
    getUsers, getUser,
    addUser, updateUser,
    deleteUser
};