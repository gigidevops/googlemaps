const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const UserService = require('../service/UserService');
const AuthService = require('../service/AuthService');

const login = async (req, res) => {
    const {mail, motDePasse} = req.body;
    const user = await UserService.getUserByMail(mail);

    if(user === null){
        return res.status(400).json({message: "Impossible de se connecter"});
    } else {
        const motDePasseValide = bcrypt.compareSync(motDePasse, user.motDePasse);

        if(!motDePasseValide){
            return res.status(400).json({message: "Impossible de se connecter"});
        } else {
            const token = await AuthService.genToken(user._id);

            if(token === null){
                return res.status(400).json({message: "Impossible de se connecter"});
            } else {
                return res.status(200).json(Object.assign({token}, user._doc));
            }
        }
    }
}

module.exports = {
    validateToken: (req, res, next) => {
        const token = req.header('x-token');
        if(!token){
            return res.status(400).json({message: 'Token has not send'});
        }
        try {
            const{ uid, nom} = jwt.verify(token, process.env.SECRET_TOKEN);
            req.uid = uid;
            req.nom = nom;
        } catch (error) {
            return res.status(400).json({message:'invalid token'})
        }
    },

    login
};