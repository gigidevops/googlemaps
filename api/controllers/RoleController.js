const RoleService = require("../service/RoleService");
const RoleModel = require("../models/RoleModel");

const addRole = async (req, res, next) => {
    const roleData = new RoleModel({
        id: req.body.id,
        scope: req.body.scope
    });

    const role = await RoleService.addRole(roleData);

    // Vérifie que l'ajout a réussi
    if(role === null){
        return res.status(400).json({message: "Erreur lors de la création d'un rôle"});
    } else {
        return res.status(200).json(role);
    }
};

const getRole = async (req, res, next) => {
    const role = await RoleService.getRole(req.params.id);

    if(role === null){
        return res.status(400).json({message: "Impossible de récupérer le rôle"});
    } else {
        return res.status(200).json(role);
    }
};

const getRoles = async (req, res, next) => {
    const roles = await RoleService.getRoles();

    if(roles === null){
        return res.status(400).json({message: "Impossible de récupérer les rôles"});
    } else {
        return res.status(200).json(roles);
    }
};

module.exports = {
    addRole, getRole, getRoles
};