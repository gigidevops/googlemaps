const mongoose = require('mongoose');
const { Schema, model} = require('mongoose');

const UserModel = Schema({
    nom: {
        type: String,
        require: true
    },
    mail:{
        type: String,
        require: true
    },
    motDePasse: {
        type: String,
        require: true
    }, 
    numeroDeSiret: {
        type: String,
        require: true
    },
    latitude: {
        type: Number,
        require: true
    },
    longitude: {
        type: Number,
        require: true
    },
    role: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('User', UserModel);