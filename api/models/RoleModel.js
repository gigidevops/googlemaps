const mongoose = require('mongoose');
const { Schema, model} = require('mongoose');

const RoleModel = Schema({
    id: {
        type: String,
        require: true
    },
    scope: {
        type: Schema.Types.Mixed,
        require: true
    }
});

module.exports = mongoose.model('Role', RoleModel);