const express = require('express');
const { addUser, getUsers, getUser, updateUser, deleteUser } = require('../controllers/UserController');
const { checkDroit } = require('../middleware/AuthMiddleware');
const router = express.Router();

// Récuperer et ajouter un utilisateur
router.get ('/', [checkDroit], getUsers);
router.post('/', [checkDroit], addUser);

router.get   ('/:id', [checkDroit], getUser   );
router.put   ('/:id', [checkDroit], updateUser);
router.delete('/:id', [checkDroit], deleteUser);

module.exports = router;
