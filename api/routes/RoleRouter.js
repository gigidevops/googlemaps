const express = require('express');
const { checkDroit } = require('../middleware/AuthMiddleware');
const {addRole, getRole, getRoles} = require("../controllers/RoleController");
const router = express.Router();

// Récuperer et ajouter un utilisateur
router.get ('/', [checkDroit], getRoles);
router.post('/', [checkDroit], addRole );

router.get('/:id', [checkDroit], getRole);

module.exports = router;
