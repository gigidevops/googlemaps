const mongoose = require('mongoose');

module.exports = () => {
    mongoose.connect(process.env.DB_CONNECT, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, (err) => {
        if(err){
            throw new Error("Erreur lors de la connexion à la base de données");
        } else {
            console.log("Connexion à la base de données réussie");
        }
    });
};