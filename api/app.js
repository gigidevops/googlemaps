//#region Les dépendances
const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');

// Configuration
const dbConnect = require('./core/database');

// Accès aux fichiers des routes
const authRouter = require('./routes/authRouter');
const userRouter = require("./routes/userRouter");
const roleRouter = require("./routes/roleRouter");
//#endregion

// Les configurations
dotenv.config(); // Configuration pour l'accès aux variables d'environnement
dbConnect(); // Configuration de la connection à la base de données

// Création de l'application
const app = express();

// Middlewares
app.use(cors()); // Sécurité Cors
app.use(helmet()); // Sécurité Helmet
app.use(bodyParser.json()); // Permet de pouvoir faire res.body

// Ajout des routes
app.use('/users' , userRouter);
app.use("/auth"  , authRouter);
app.use("/role"  , roleRouter);

// Lancement du serveur
app.listen(3001, () => console.log('Server starting'));