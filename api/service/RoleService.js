const RoleModel = require("../models/RoleModel");

const getRoles = async () => new Promise((resolve, reject) => {
    RoleModel.find((err, res) => {
        if(err){
            resolve(null);
        } else {
            resolve(res);
        }
    });
});

const getRole = (id) => {
    return new Promise((resolve, reject) => {
        RoleModel.findOne({id}, (err, res) => {
            if(err){
                resolve(null);
            } else {
                resolve(res);
            }
        });
    });
};

const addRole = (role) => new Promise((resolve, reject) => {
    RoleModel.findOne({id: role.id}, (err, res) => {
        if(err){
            resolve(null);
        } else {
            if(res !== null){
                resolve(null);
            } else {
                RoleModel.create(role, (err, res) => {
                    if(err){
                        resolve(null);
                    } else {
                        resolve(res);
                    }
                });
            }
        }
    });
});

module.exports = {
    getRole, getRoles, addRole
};