const UserModel = require("../models/UserModel");
const AuthService = require("./AuthService");

/**
 * Permet de récupérer tous les utilisateurs
 * @return {ModelUser | null} Tous les utilisateurs. Null, si la requête échoue.
 */
const getUsers = async () => new Promise((resolve, reject) => {
    UserModel.find((err, res) => {
        if(err){
            resolve(null);
        } else {
            resolve(res);
        }
    });
});

/**
 * Permet de récupérer un seul utilisateur grâce à son identifiant
 * @param {Number} id IDentifiant de l'utilsiateur que l'on souhaite récupérer
 */
const getUserById = (id) => new Promise((resolve, reject) => {
    UserModel.findById(id, (err, res) => {
        if(err){
            resolve(null);
        } else {
            resolve(res);
        }
    });
});

const getUserByMail = (mail) => new Promise((resolve, reject) => {
    UserModel.findOne({mail}, (err, res) => {
        if(err){
            resolve(null);
        } else {
            resolve(res);
        }
    });
});

/**
 * Permet de créer un nouvel utilisateur
 * @param {UserModel} user L'utilisateur à ajouter
 */
const addUser = (user) => new Promise((resolve, reject) => {
    UserModel.findOne({mail: user.mail}, (err, res) => {
        if(err){
            resolve(null);
        } else {

            // Un utilisateur avec cet email existe déjà
            if(res !== null){
                resolve(null);
            } else {
                // Cryptage du mote de passe
                user.motDePasse = AuthService.cryptageMdp(user.motDePasse);

                UserModel.create(user, (err, res) => {
                    if(err){
                        resolve(null);
                    } else {
                        resolve(res);
                    }
                });
            }
        }
    });
});

/**
 * Permet de modifier un utilisateur
 * @param {UserModel} user Nouvel utilisateur
 */
const updateUser = (id, user) => new Promise((resolve, reject) => {
    UserModel.updateOne({_id: id}, user, (err, res) => {
        if(err){
            resolve(null);
        } else {
            resolve(user);
        }
    });
});

/**
 * PErmet de supprimer un utilisateur grâce à son identifiant
 * @param {String} id Identifiant de l'utilisateur
 */
const deleteUser = (id) => new Promise((resolve, reject) => {
    getUserById(id).then(user => {
        if(user === null){
            resolve(null);
        } else {
            UserModel.deleteOne({_id: id}, (err, res) => {
                if(err){
                    resolve(null);
                } else {
                    resolve(user);
                }
            });
        }
    });
});

module.exports = {
    addUser, getUsers, getUserById, getUserByMail, deleteUser, updateUser
};