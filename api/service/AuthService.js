const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const cryptageMdp = (motDePasse) => {
    return bcrypt.hashSync(motDePasse, bcrypt.genSaltSync());
};

const genToken = (id) => new Promise((resolve, reject) => {
    jwt.sign({id}, process.env.SECRET_TOKEN, {expiresIn: '48h'}, (error, token) => {
        if(error){
            reject(null);
        } else {
            resolve(token);
        }
    });
});

module.exports = {
    cryptageMdp, genToken
};