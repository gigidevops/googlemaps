import React, { useEffect, useState } from "react";
import UserEntity from "../../api/user/UserEntity";
import {Map, Marker, Popup, TileLayer} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "./MapModule.scss";
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from "leaflet";
import UserService from "../../api/user/UserService";
import { Modal } from "react-bootstrap";

export interface MapModuleProps{
    user: UserEntity,
    onClickMap: (latitude: number, longitude: number) => void,
    estSynchronise?: boolean,
    setEstSynchronise: React.Dispatch<React.SetStateAction<boolean>>
}

export default (props: MapModuleProps) => {

    //#region Configurations

    // Configuration des icônes
    L.Marker.prototype.options.icon = L.icon({
        iconUrl: icon,
        shadowUrl: iconShadow
    });

    // Configuration du centre de la carte
    let center = {
        lat: 45.630225, 
        lng: 0.146125
    };
    //#endregion

    let firstRender = true;

    //#region Les états
    const [users, setUsers] = useState<UserEntity[]>([]);
    const [user, setUser] = useState<UserEntity | null>(null);
    //#endregion

    //#region Les effets
    useEffect(() => {
        if(firstRender || (props.estSynchronise !== undefined && props.estSynchronise === true)){
            if(props.user.token !== null){
                UserService.getUsers(props.user.token).then(u => {
                    if(u !== null){
                        setUsers(u);
                    }
    
                    props.setEstSynchronise(true);
                });
            }
        }
    }, [props.estSynchronise]);
    //#endregion

    //#region Les fonctions
    const onClickMarker = (id: string) => {
        const myUser = users.find(u => u.id === id);

        if(myUser !== undefined){
            setUser(myUser);
        }
    };

    const onClickMap = (evt: {latlng: L.LatLng}) => {
        const latlng = evt.latlng;
        const [latitude, longitude] = [latlng.lat, latlng.lng];
        
        props.onClickMap(latitude, longitude);
    }
    //#endregion

    return <div id="mapModule">
        <Map center={center} zoom={13}
            onClick={onClickMap}
        >
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            />
            {users.map(u => {
                if(u.latitude !== null && u.longitude !== null){
                    return <Marker key={`user-${u.id}`}
                        position={[u.latitude, u.longitude]}
                        onClick={() => onClickMarker(u.id)}
                    />
                }
            })}
        </Map>
        <Modal show={user !== null} onHide={() => setUser(null)} centered>
            <Modal.Header closeButton>
                <Modal.Title>Détails de l'utilisateur</Modal.Title>
            </Modal.Header>
            <Modal.Body id="panelInfos">
                {user !== null && <>
                             Nom: {user.nom} 
                    <br></br>Adresse mail: {user.mail}
                    <br></br>N° de Siret: {user.numeroDeSiret}
                    <br></br>latitude: {user.latitude}
                    <br></br>Longitude: {user.longitude}
                    <br></br>Rôle: {user.role}
                </>}
            </Modal.Body>
        </Modal>
    </div>;
}