import React, { useEffect, useState } from "react";
import UserEntity from "../../api/user/UserEntity";
import UserService from "../../api/user/UserService";
import UtilisateurGrid from "../grid/UtilisateurGrid";
import "./UtilisateurModule.scss";

export interface UtilisateurModuleProps{
    setAfficherInscriptionModal: React.Dispatch<React.SetStateAction<boolean>>,
    user: UserEntity
}

export default (props: UtilisateurModuleProps) => {

    return <div id="utilisateurModule">
        <UtilisateurGrid 
            setAfficherInscriptionModal={props.setAfficherInscriptionModal}
            user={props.user}
        />
    </div>;
}