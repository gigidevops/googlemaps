import { MenuItem } from 'primereact/components/menuitem/MenuItem';
import { Menubar } from 'primereact/menubar';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import UserEntity from '../api/user/UserEntity';
import { InscriptionFormValues } from './form/InscriptionForm';
import "./Menu.scss";

export interface MenuProps{
    setAfficherConnexionModal: React.Dispatch<React.SetStateAction<boolean>>,
    setAfficherInscriptionModal: React.Dispatch<React.SetStateAction<boolean>>,
    setInscriptionValues: React.Dispatch<React.SetStateAction<InscriptionFormValues | null>>,
    user: UserEntity | null,
    setUser: React.Dispatch<React.SetStateAction<UserEntity | null>>
}

export default (props: MenuProps) => {

    const history = useHistory();

    //#region Les effets
    const [items, setItems] = useState<MenuItem[]>([]);
    //#endregion

    //#region Les effets

    // Permet de charger les items du menu
    useEffect(() => {
        let items: MenuItem[] = [{
            label: "Accueil",
            icon: "pi pi-home",
            command: () => {
                history.push("/");
            }
        }];

        if(props.user !== null){
            items = [...items, ...[{
                label: "Utilisateurs",
                icon: "pi pi-users",
                command: () => {
                    history.push("/users");
                }
            }, {
                label: "Carte",
                icon: "pi pi-globe",
                command: () => {
                    history.push("/map");
                }
            }]];
        }

        setItems(items);
    }, [props.user]);
    //#endregion

    return <Menubar id="menu"
        model={items}
        //@ts-ignore
        end={props.user === null
            ? <>
                <Button variant="outline-info" 
                    onClick={() => {
                        props.setAfficherInscriptionModal(true);
                        props.setInscriptionValues(null);
                    }}
                >Inscription</Button>{" "}
                <Button variant="outline-success"
                    onClick={() => props.setAfficherConnexionModal(true)}
                >Connexion</Button>
            </>
            : <>
                <Button variant="danger"
                    onClick={() => props.setUser(null)}
                >Déconnexion</Button>
            </>
        }
    />
}