import React, { useEffect, useState } from "react"
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import UserEntity from "../../api/user/UserEntity";
import UserService from "../../api/user/UserService";
import { Toolbar } from 'primereact/toolbar';
import { Button } from "react-bootstrap";

export interface UtilisateurGridProps{
    setAfficherInscriptionModal: React.Dispatch<React.SetStateAction<boolean>>,
    user: UserEntity
}

export default (props: UtilisateurGridProps) => {

    //#region Les états
    const [users, setUsers] = useState<UserEntity[]>([]);
    const [userSelectionne, setUserSelectionne] = useState<UserEntity | null>(null);
    const [estSynchronise, setEstSynchronise] = useState(false);
    //#endregion

    //#region Les effets
    
    // Chargement des utilisateurs
    useEffect(() => {
        if(!estSynchronise){
            if(props.user.token !== null){
                UserService.getUsers(props.user.token).then(u => {
                    if(u !== null){
                        setUsers(u);
                    }
    
                    setEstSynchronise(true);
                });
            }
        }
    }, [estSynchronise]);
    //#endregion

    return <DataTable
        value={users}

        header={<Toolbar style={{padding: 0, border: "none"}}
            left={() => <>
                <Button variant="primary" onClick={() => {
                    props.setAfficherInscriptionModal(true);
                }} disabled={props.user.role !== "A"}>Ajouter</Button><span style={{marginRight: "10px"}} />
                <Button variant="danger"
                    onClick={() => {
                        if(userSelectionne !== null && props.user.token !== null){
                            UserService.delUser(userSelectionne.id, props.user.token).then(u => {
                                setEstSynchronise(false);
                            });
                        }
                    }}
                    disabled={userSelectionne === null}
                >Supprimer</Button>
            </>}
        />}

        selectionMode="single" selection={userSelectionne} onSelectionChange={e => {
            setUserSelectionne(e.value);
        }}
    >
        <Column field="id" header="#" style={{width: "220px"}}/>
        <Column field="mail" header="Email" />
        <Column field="nom" header="Nom" />
        <Column field="numeroDeSiret" header="Numérot de siret" />
        <Column field="latitude" header="Latitude" />
        <Column field="longitude" header="Longitude" />
        <Column field="role" header="role" />
    </DataTable>
}