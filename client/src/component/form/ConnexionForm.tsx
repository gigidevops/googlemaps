import React from "react";
import * as yup from "yup";
import {Formik} from "formik";
import {Button, Col, Form} from "react-bootstrap";
import UserEntity from "../../api/user/UserEntity";
import UserService from "../../api/user/UserService";

const schema = yup.object({
  mail: yup.string().default("")
    .required("Champs obligatoire")
    .email("Doit être une adresse email"),
  motDePasse: yup.string().default("")
    .required("Champs obligatoire")
    .min(8, "Doit être d'une longueur >= 8")
}).defined();

export interface ConnexionFormProps{
  setUser: React.Dispatch<React.SetStateAction<UserEntity | null>>,
  afterSubmit?: () => void
}

export default (props: ConnexionFormProps) => {

  //#region Les fonctions
  const submit = (values: yup.InferType<typeof schema>) => {

    UserService.login(values.mail, values.motDePasse).then(user => {
      props.setUser(user);

      if(props.afterSubmit !== undefined){
        props.afterSubmit();
      }
    });
  }
  //#endregion

  return <Formik
    validationSchema={schema}
    onSubmit={submit}
    initialValues={schema.default()}
  >
    {({
      handleChange,
      handleSubmit,
      values,
      touched,
      errors
    }) => (
      <Form noValidate onSubmit={(evt) => handleSubmit(evt as any)}>
        <Form.Row>
          <Form.Group as={Col} controlId="mail">
            <Form.Label>Identifiant</Form.Label>
            <Form.Control
              type="email"
              name="mail"
              placeholder="mon_email@gmail.com"
              value={values.mail}
              onChange={handleChange}
              isValid={touched.mail && !errors.mail}
              isInvalid={!!errors.mail}
            />
            <Form.Control.Feedback type="invalid">
              {errors.mail}
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group as={Col} controlId="motDePasse">
            <Form.Label>Mot de passe</Form.Label>
            <Form.Control
              type="password"
              name="motDePasse"
              value={values.motDePasse}
              onChange={handleChange}
              isValid={touched.motDePasse && !errors.motDePasse}
              isInvalid={!!errors.motDePasse}
            />
            <Form.Control.Feedback type="invalid">
              {errors.motDePasse}
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        <Button type="submit">Connexion</Button>
      </Form>
    )}
  </Formik>;
}