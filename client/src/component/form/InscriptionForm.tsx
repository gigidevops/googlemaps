import React, { useEffect, useState } from "react";
import * as yup from "yup";
import {Formik} from "formik";
import {Button, Col, Form} from "react-bootstrap";
import UserEntity from "../../api/user/UserEntity";
import UserCreateEntity from "../../api/user/UserCreateEntity";
import UserService from "../../api/user/UserService";
import roleData from "../../data/roleData";

const schema = yup.object({
    nom: yup.string()
      .default("")
      .required("Champs obligatoire"),
    mail: yup.string()
      .default("")
      .required("Champs obligatoire")
      .email("Doit être une adresse mail valide"),
    motDePasse: yup.string()
      .default("")
      .required("Champs obligatoire")
      .min(8, "Doit être d'une longueur >= 8"),
    numeroDeSiret: yup.string()
      .default("")
      .required("Champs obligatoire"),
    latitude: yup.number()
      .default(0)
      .required("Champs obligatoire")
      .typeError("Doit être un nombre"),
    longitude: yup.number()
      .default(0)
      .required("Champs obligatoire")
      .typeError("Doit être un nombre"),
    role: yup.string()
      .default("A")
      .required("Champs obligatoire"),
  }).defined();

export interface InscriptionFormValues{
  latitude: number,
  longitude: number
}

export interface InscriptionFormProps{
  setUser: React.Dispatch<React.SetStateAction<UserEntity | null>>,
  user: UserEntity | null,
  afterSubmit?: () => void,
  values?: InscriptionFormValues
}

export default (props: InscriptionFormProps) => {

  //#region Les états
  const [values, setValues] = useState<yup.InferType<typeof schema>>(schema.default());
  //#endregion

  //#region Les fonctions
  const submit = (values: yup.InferType<typeof schema>) => {

    const newUser = new UserCreateEntity("", values.mail, values.nom, values.numeroDeSiret, values.role, values.motDePasse);
    newUser.latitude = values.latitude;
    newUser.longitude = values.longitude;

    if(props.user !== null && props.user.token !== null){
      UserService.createUser(newUser, props.user.token).then(user => {
        props.setUser(user);
  
        if(props.afterSubmit !== undefined){
          props.afterSubmit();
        }
      });
    }
  }
  //#endregion

  //#region Les effets
  useEffect(() => {
    let vals = schema.default();

    if(props.values !== undefined){
      vals = {...vals, ...props.values};
    }

    console.log(vals);
    setValues(vals);
  }, [props.values]);
  //#endregion

  return <Formik
    validationSchema={schema}
    onSubmit={submit}
    initialValues={values}
    enableReinitialize={true}
  >
    {({
      handleChange,
      handleSubmit,
      values,
      touched,
      errors
    }) => (
      <Form noValidate onSubmit={(evt) => handleSubmit(evt as any)}>
        
        {/* Nom */}
        <Form.Row>
          <Form.Group as={Col} controlId="nom">
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="text"
              name="nom"
              value={values.nom}
              onChange={handleChange}
              isValid={touched.nom && !errors.nom}
              isInvalid={!!errors.nom}
            />
            <Form.Control.Feedback type="invalid">{errors.nom}</Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        {/* Mail */}
        <Form.Row>
        <Form.Group as={Col} controlId="mail">
          <Form.Label>Identifiant</Form.Label>
          <Form.Control
            type="email"
            name="mail"
            placeholder="exemple@mail.com"
            value={values.mail}
            onChange={handleChange}
            isValid={touched.mail && !errors.mail}
            isInvalid={!!errors.mail}
          />
          <Form.Control.Feedback type="invalid">
            {errors.mail}
          </Form.Control.Feedback>
        </Form.Group>
        </Form.Row>

        {/* Mot de passe */}
        <Form.Row>
        <Form.Group as={Col} controlId="motDePasse">
            <Form.Label>Mot de passe</Form.Label>
            <Form.Control
              type="password"
              name="motDePasse"
              value={values.motDePasse}
              onChange={handleChange}
              isValid={touched.motDePasse && !errors.motDePasse}
              isInvalid={!!errors.motDePasse}
            />
            <Form.Control.Feedback type="invalid">{errors.motDePasse}</Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        {/* Numero de SIRET */}
        <Form.Row>
        <Form.Group as={Col} controlId="numeroDeSiret">
            <Form.Label>N° de SIRET</Form.Label>
            <Form.Control
              type="text"
              name="numeroDeSiret"
              value={values.numeroDeSiret}
              onChange={handleChange}
              isValid={touched.numeroDeSiret && !errors.numeroDeSiret}
              isInvalid={!!errors.numeroDeSiret}
            />
            <Form.Control.Feedback type="invalid">{errors.numeroDeSiret}</Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        {/* Latitude et Longitude*/}
        <Form.Row>
          <Form.Group as={Col} controlId="latitude">
            <Form.Label>Latitude</Form.Label>
            <Form.Control
              type="text"
              placeholder="Latitude"
              name="latitude"
              value={values.latitude}
              onChange={handleChange}
              isValid={touched.latitude && !errors.latitude}
              isInvalid={!!errors.latitude}
            />
            <Form.Control.Feedback type="invalid">
              {errors.latitude}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group as={Col} controlId="longitude">
            <Form.Label>Longitude</Form.Label>
            <Form.Control
              type="text"
              placeholder="Longitude"
              name="longitude"
              value={values.longitude}
              onChange={handleChange}
              isValid={touched.longitude && !errors.longitude}
              isInvalid={!!errors.longitude}
            />
            <Form.Control.Feedback type="invalid">
              {errors.longitude}
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        {/*  Rôle*/}
        <Form.Row>
          <Form.Group as={Col} controlId="role">
              <Form.Label>Rôles</Form.Label>
              <Form.Control as="select" custom
                name="role"
                value={values.role}
                onChange={handleChange}
                isValid={touched.role && !errors.role}
                isInvalid={!!errors.role}
              >
              {roleData.map(role => {
                return <option key={`role_${role.id}`} value = {role.id}>
                  { role.role}
                </option>
              })}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                {errors.role}
              </Form.Control.Feedback>
            </Form.Group>
        </Form.Row>

        <Button type="submit">S'inscrire</Button>
      </Form>
    )}
  </Formik>;
}