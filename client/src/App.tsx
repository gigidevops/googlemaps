import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import UserEntity from './api/user/UserEntity';
import ConnexionForm from './component/form/ConnexionForm';
import Menu from './component/Menu';
import {BrowserRouter, useLocation} from "react-router-dom";
import Router from "./route/Router";
import InscriptionForm, { InscriptionFormValues } from './component/form/InscriptionForm';
import { setIn } from 'formik';
import { propTypes } from 'react-bootstrap/esm/Image';

export default () => {

    //#region Les états
    const [afficherConnexionModal, setAfficherConnexionModal] = useState(false);
    const [afficherInscriptionModal, setAfficherInscriptionModal] = useState(false);
    const [incriptionValues, setInscriptionValues] = useState<InscriptionFormValues | null>(null);

    // Utilisateur actuellement connecté
    const [user, setUser] = useState<UserEntity | null>(null);

    const [estSynchronise, setEstSynchronise] = useState(true);
    //#endregion

    //#region Les fonctions
    const onClickMap = (latitude: number, longitude: number) => {
        if(user !== null && user.role === "A"){
            setInscriptionValues({
                latitude, longitude
            });
            setAfficherInscriptionModal(true);
        }
    }
    //#endregion

    return <BrowserRouter>
        
        {/* Barre de navigation */}
        <Menu 
            setAfficherConnexionModal={setAfficherConnexionModal}
            setAfficherInscriptionModal={setAfficherInscriptionModal}
            setInscriptionValues={setInscriptionValues}
            user={user} setUser={setUser}
        />
        
        {/* Gestion du routage */}
        <Router 
            onClickMap={onClickMap}
            estSynchronise={estSynchronise}
            setEstSynchronise={setEstSynchronise}
            setAfficherInscriptionModal={setAfficherInscriptionModal}
            user={user}
        />

        {/* Modals */}
        <Modal show={afficherConnexionModal} onHide={() => setAfficherConnexionModal(false)} centered>
            <Modal.Header closeButton>
                <Modal.Title>Formulaire de connexion</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <ConnexionForm 
                    setUser={setUser}
                    afterSubmit={() => setAfficherConnexionModal(false)}
                />
            </Modal.Body>
        </Modal>

        <Modal show={afficherInscriptionModal} onHide={() => setAfficherInscriptionModal(false)} centered>
            <Modal.Header closeButton>
                <Modal.Title>Formulaire d'inscription</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <InscriptionForm 
                    setUser={setUser}
                    afterSubmit={() => {
                        setAfficherInscriptionModal(false);
                        setEstSynchronise(false);
                    }}
                    values={incriptionValues === null ? undefined : incriptionValues}
                    user={user}
                />
            </Modal.Body>
        </Modal>
    </BrowserRouter>;
}
