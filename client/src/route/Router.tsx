import React from "react";
import {Route, Switch} from "react-router-dom";
import UserEntity from "../api/user/UserEntity";
import HomeModule from "../component/module/HomeModule";
import MapModule from "../component/module/MapModule";
import UtilisateurModule from "../component/module/UtilisateurModule";

export interface RouterProps{
    onClickMap: (latitude: number, longitude: number) => void,
    estSynchronise?: boolean,
    setEstSynchronise: React.Dispatch<React.SetStateAction<boolean>>,
    setAfficherInscriptionModal: React.Dispatch<React.SetStateAction<boolean>>,
    user: UserEntity | null
}

export default (props: RouterProps) => {

    return <Switch>
        <Route exact path="/">
            <HomeModule />
        </Route>
        {props.user !== null && <>
            <Route path="/users">
                <UtilisateurModule 
                    setAfficherInscriptionModal={props.setAfficherInscriptionModal}
                    user={props.user}
                />
            </Route>
            <Route path="/map">
                <MapModule 
                    onClickMap={props.onClickMap}
                    estSynchronise={props.estSynchronise}
                    setEstSynchronise={props.setEstSynchronise}
                    user={props.user}
                />
            </Route>
        </>}
    </Switch>;
}