import UserCreateEntity from "./UserCreateEntity";
import UserEntity from "./UserEntity";


export default class UserMap{

    static jsonToEntity(json: any){
        const user = new UserEntity(
            json._id, json.mail, json.nom, json.numeroDeSiret, json.role
        );

        user.latitude = json.latitude;
        user.longitude = json.longitude;

        if(json.token !== undefined){
            user.token = json.token;
        }

        return user;
    }

    static entityToJson(user : UserCreateEntity){
        const json = {nom: user.nom, mail: user.mail,
            motDePasse: user.motDePasse, 
            numeroDeSiret: user.numeroDeSiret,
            longitude: user.longitude,
            latitude: user.latitude,
            role: user.role
        }
        return json;
    }
}