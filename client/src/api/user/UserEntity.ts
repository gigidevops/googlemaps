export default class UserEntity{
    
    id: string = "";
    mail: string = "";
    nom: string = "";
    role: string = "";
    numeroDeSiret: string = "";

    token: string | null = null;
    latitude: number | null = null;
    longitude: number | null = null;

    constructor(id: string, mail: string, nom: string, numeroDeSiret: string, role: string){
        this.id = id;
        this.nom = nom;
        this.mail = mail;
        this.numeroDeSiret = numeroDeSiret;
        this.role = role;
    }
}