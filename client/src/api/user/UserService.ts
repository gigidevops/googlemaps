import ApiConfiguration from "../../configuration/ApiConfiguration";
import UserCreateEntity from "./UserCreateEntity";
import UserEntity from "./UserEntity";
import UserMap from "./UserMap";

export default class UserService{

    static createUser(user: UserCreateEntity, token: string = ""){
        return new Promise<UserCreateEntity | null>((resolve, reject) => {
            fetch(`${ApiConfiguration.HOST}/users`,{
                method: "POST",
                headers: new Headers({
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }),
                body: JSON.stringify(UserMap.entityToJson(user))
            }).then(resp => {
                if(resp.status !== 201){
                    resolve(null);
                } else {
                    resp.json().then(json => {
                        user.id = json._id;
                        resolve(user);
                    }).catch(_ => resolve(null));
                }
            }).catch(_ => resolve(null));
        });
    }

    static login(mail: string, motDePasse: string){
        return new Promise<UserEntity | null>((resolve, reject) => {
            fetch(`${ApiConfiguration.HOST}/auth/login`, {
                method: "POST",
                headers: new Headers({
                    "Content-Type": "application/json"
                }),
                body: JSON.stringify({mail, motDePasse})
            }).then(resp => {
                if(resp.status !== 200){
                    resolve(null);
                } else {
                    resp.json().then(json => {
                        resolve(UserMap.jsonToEntity(json));
                    }).catch(_ => resolve(null));
                }
            }).catch(_ => resolve(null));
        });
    }

    static getUsers(token = ""){
        return new Promise<UserEntity[] | null>((resolve, reject) => {
            fetch(`${ApiConfiguration.HOST}/users`, {
                headers: new Headers({
                    "Authorization": `Bearer ${token}`
                })
            }).then(resp => {
                if(resp.status !== 200){
                    resolve(null);
                } else {
                    resp.json().then((users: any[]) => {
                        resolve(users.map(u => {
                            return UserMap.jsonToEntity(u);
                        }));
                    }).catch(_ => resolve(null));
                }
            }).catch(_ => resolve(null));
        });
    }

    static delUser(id: string, token = ""){
        return new Promise<UserEntity | null>((resolve, reject) => {
            fetch(`${ApiConfiguration.HOST}/users/${id}`, {
                method: "DELETE",
                headers: new Headers({
                    "Authorization": `Bearer ${token}`
                })
            }).then(resp => {
                if(resp.status !== 200){
                    resolve(null);
                } else {
                    resp.json().then(user => {
                        resolve(UserMap.jsonToEntity(user));
                    }).catch(_ => resolve(null));
                }
            }).catch(_ => resolve(null));
        });
    }
}