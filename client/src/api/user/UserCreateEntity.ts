import UserEntity from "./UserEntity";

export default class UserCreateEntity extends UserEntity{
    motDePasse: string = "";
    constructor(id: string, mail: string, nom: string, numeroDeSiret: string, role: string, motDePasse: string){
        super(id, mail, nom, numeroDeSiret, role);
        this.motDePasse = motDePasse;
    }
}